inputmanager
============

A device-agnostic input-management system for the [Monkey programming language](https://github.com/blitz-research/monkey). This framework may be massively changed later down the road. If such an event happens, your code may not be supported; please keep this in mind.
